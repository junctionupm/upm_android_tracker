package com.voldemarich.upftrack.app.bt_stuff;

import android.bluetooth.le.BluetoothLeScanner;
import java.util.HashSet;

/**
 * Created by voldemarich on 26.11.2016.
 */
public class ScanningThread extends Thread {

    BluetoothLeScanner bluetoothLeScanner;
    ReturningScanCallback rsc;
    HashSet<PolledDevice> scanResults;

    public ScanningThread(BluetoothLeScanner bls, HashSet<PolledDevice> lsr) {
        super();
        bluetoothLeScanner = bls;
        scanResults = lsr;
    }

    @Override
    public void run() {
        rsc = new ReturningScanCallback(scanResults);
        bluetoothLeScanner.startScan(rsc);
    }
}

//This thread is useless, planned for short-time scan runs (now run goes all the time)
