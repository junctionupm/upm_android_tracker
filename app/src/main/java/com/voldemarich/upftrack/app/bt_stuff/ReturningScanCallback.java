package com.voldemarich.upftrack.app.bt_stuff;

import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by voldemarich on 26.11.2016.
 */
public class ReturningScanCallback extends ScanCallback {

    HashSet<PolledDevice> scanResults;

    public ReturningScanCallback(HashSet<PolledDevice> scanResults) {
        super();
        this.scanResults = scanResults;
    }

    @Override
    public void onScanResult(int callbackType, ScanResult result) {
        PolledDevice p = new PolledDevice(result);
        if (! scanResults.add(p)){
            scanResults.remove(p);
            scanResults.add(p);
        }
    }

//    @Override
//    public void onBatchScanResults(List<ScanResult> results) {
//        scanResults.addAll(results);
//    }

    @Override
    public void onScanFailed(int errorCode){
        try {
            throw new Exception("Scan failed, kek, go turn on BT"); //todo okay exception handling
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashSet<PolledDevice> getScanResults() {
        return scanResults;
    }
}
