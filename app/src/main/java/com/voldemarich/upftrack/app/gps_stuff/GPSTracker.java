package com.voldemarich.upftrack.app.gps_stuff;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * Created by voldemarich on 26.11.2016.
 */
public class GPSTracker {
    LocationManager locationManager;
    LocationListener locationListener;

    Location mylocation = new Location(LocationManager.PASSIVE_PROVIDER);

    public GPSTracker(Context context){
        locationManager = (LocationManager)(context.getSystemService(Context.LOCATION_SERVICE));
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mylocation = location;
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }

        };
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, locationListener);
    }

    public Location getLocation() {
        return mylocation;
    }
}
