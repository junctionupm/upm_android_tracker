package com.voldemarich.upftrack.app.mqtt_stuff;

import android.location.Location;
import android.util.Log;
import com.voldemarich.upftrack.app.bt_stuff.PolledDevice;

import java.util.HashSet;

/**
 * Created by voldemarich on 26.11.2016.
 */
public class MQTTThread extends Thread {

    HashSet<PolledDevice> detectedDevices;

    Location location;

    MQTTConnector mqttConnector;


    public MQTTThread(HashSet<PolledDevice> hpd, Location loc, MQTTConnector mttq) {
        super();
        detectedDevices = hpd;
        location = loc;
        mqttConnector = mttq;
    }

    private String objToJson(){
        String a = "{\"tracker_id\":1,\"reels\":[";
        for (PolledDevice p : detectedDevices){
            a+="\""+p.getDev_mac()+"\",";
        }
        a = a.substring(0, a.length()-2);
        a += "],\"lat\":"+location.getLatitude()+",\"lon\":"+location.getLongitude()+
                ",\"speed\":"+location.getSpeed()*3.6+"}";
        return a;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            mqttConnector.start_monitor_delivery();
            while (true) {
                mqttConnector.send_data(objToJson());
                Thread.sleep(2000);
            }
        }
        catch (Exception e){
            Log.e("pizdez", e.getMessage());
        }
    }
}
