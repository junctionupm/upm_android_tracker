package com.voldemarich.upftrack.app.bt_stuff;

import java.security.Timestamp;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by voldemarich on 26.11.2016.
 */
public class BLEMonitorThread extends Thread {

    HashSet<PolledDevice> polledDevices;

    public BLEMonitorThread(HashSet<PolledDevice> pd) {
        super();
        polledDevices = pd;
    }

    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep(90000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            HashSet<PolledDevice> lostDevices = new HashSet();
            for (PolledDevice p : polledDevices){
                if((int)(new Date().getTime()/1000) - p.getSecs() > 90) { //magic value
                    lostDevices.add(p);
                    }
            }
            for (PolledDevice lp : lostDevices){
                polledDevices.remove(lp);
            }

        }
    }
}
