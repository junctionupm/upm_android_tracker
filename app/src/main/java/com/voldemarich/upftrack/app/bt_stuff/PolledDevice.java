package com.voldemarich.upftrack.app.bt_stuff;

import android.bluetooth.le.ScanResult;

/**
 * Created by voldemarich on 26.11.2016.
 */
public class PolledDevice {
    String dev_name;
    String dev_mac;
    long secs;
    int rssi;

    public PolledDevice(ScanResult o){
        dev_name = o.getDevice().getName();
        dev_mac = o.getDevice().getAddress();
        secs = o.getTimestampNanos()/1000000000;
        rssi = o.getRssi();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PolledDevice that = (PolledDevice) o;

        if (dev_name != null ? !dev_name.equals(that.dev_name) : that.dev_name != null) return false;
        return dev_mac.equals(that.dev_mac);

    }

    public boolean equals(ScanResult o){
        return this.dev_mac.equals(o.getDevice().getAddress()) && this.dev_name.equals(o.getDevice().getName());
    }

    @Override
    public int hashCode() {
        int result = dev_name != null ? dev_name.hashCode() : 0;
        result = 31 * result + dev_mac.hashCode();
        return result;
    }

    public String getDev_name() {
        return dev_name;
    }

    public String getDev_mac() {
        return dev_mac;
    }

    public long getSecs() {
        return secs;
    }

    public int getRssi() {
        return rssi;
    }

    //    public boolean update(ScanResult sr){
//        if (this.equals(sr)){
//            secs = sr.getTimestampNanos();
//            rssi = sr.getRssi();
//            return true;
//        }
//        else return false;
//    }

    @Override
    public String toString(){
        return "{\"dev_name\":\""+dev_name+"\",\"dev_mac\":\""+dev_mac+"\",\"rssi\":"+rssi+",\"timestamp\":"+(int)(secs)+",\"trackerid\":1}";
    }
}
