package com.voldemarich.upftrack.app.bt_stuff;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;

import java.util.*;

/**
 * Created by voldemarich on 26.11.2016.
 */
public class BLEManager {

    BluetoothAdapter bluetoothAdapter;
    BluetoothLeScanner bluetoothLeScanner;
    HashSet<PolledDevice> scanResults = new HashSet<>();

    public BLEManager(Context context){
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
    }

    public void startProximityScan() throws InterruptedException {
        ScanningThread th = new ScanningThread(bluetoothLeScanner, scanResults);
        BLEMonitorThread bmth = new BLEMonitorThread(scanResults);
        th.start();
        bmth.start();
    }

    public HashSet<PolledDevice> getScanResults() {
        return scanResults;
    }

}
