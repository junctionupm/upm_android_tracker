package com.voldemarich.upftrack.app;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.voldemarich.upftrack.app.bt_stuff.BLEManager;
import com.voldemarich.upftrack.app.bt_stuff.PolledDevice;
import com.voldemarich.upftrack.app.gps_stuff.GPSTracker;
import com.voldemarich.upftrack.app.mqtt_stuff.MQTTConnector;
import com.voldemarich.upftrack.app.mqtt_stuff.MQTTThread;

import java.util.HashSet;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE Not Supported", Toast.LENGTH_SHORT).show();
            this.finish();
        }
        final BLEManager bl = new BLEManager(this);
        try {
            bl.startProximityScan();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GPSTracker gps = new GPSTracker(this);
        try{
            MQTTThread connthread = new MQTTThread(bl.getScanResults(), gps.getLocation(), new MQTTConnector());
            connthread.start();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Button topbutton = (Button)(this.findViewById(R.id.topbutton));
        final TextView tw = (TextView)(this.findViewById(R.id.tw));
        topbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }



}
