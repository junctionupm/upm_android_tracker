package com.voldemarich.upftrack.app.mqtt_stuff;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;

import java.net.URISyntaxException;

/**
 * Created by voldemarich on 26.11.2016.
 */
public class MQTTConnector extends MQTT {

    public static String host = "tcp://192.168.43.86:1883";

    BlockingConnection connection;

    public MQTTConnector() throws URISyntaxException {
        super();
        this.setHost(host);
        connection = this.blockingConnection();
    }

    public void start_monitor_delivery() throws Exception {
        connection.connect();
    }

    public void send_data(String toSend) throws Exception {
        connection.publish("upm/reels", toSend.getBytes(), QoS.AT_LEAST_ONCE, false);
    }
}
